package com.rudyr.research.io.casebook.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
@AllArgsConstructor
public class Book {
	long id;
	String name;
	String author;
}
