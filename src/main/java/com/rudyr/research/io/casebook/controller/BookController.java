package com.rudyr.research.io.casebook.controller;

import com.rudyr.research.io.casebook.model.Book;
import com.rudyr.research.io.casebook.services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    BookServices bookServices;

    @GetMapping("/Books")
    public List<Book> getBook(){
        return bookServices.getAllBooks();
    }
}
