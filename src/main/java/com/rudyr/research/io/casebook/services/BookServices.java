package com.rudyr.research.io.casebook.services;

import com.rudyr.research.io.casebook.model.Book;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BookServices {

    public List<Book> getAllBooks(){
        return Arrays.asList(new Book(1,"BigData","john doe"), new Book(2,"microservices","rudy"));
    }
}
