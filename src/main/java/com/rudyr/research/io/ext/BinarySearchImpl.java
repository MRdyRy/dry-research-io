package com.rudyr.research.io.ext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rudyr.research.io.ext.impl.SortAlgorithm;

@Component
public class BinarySearchImpl {
	
	@Autowired
	private SortAlgorithm sortAlgorithm;

	public int binarySearch(int[] numbers, int numberToSearch) {
		//logic to switch sorting algorithm
		int [] data  = sortAlgorithm.sort(numbers);
		System.out.println(sortAlgorithm);	
		return 0;
	}
}
