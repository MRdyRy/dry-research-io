package com.rudyr.research.io.ext.impl;

//bikin interface supaya nanti bisa di implement di logic, nanti semua yg implement interface bisa di buat object nya jadi bisa dinamis
/*
 * contoh case: misal pada pengembangan awal hanya ada buble sort, di pengembangan selanjutnya ada penambahan quick sort
 * maka supaya sortingnya bisa dinamis, perlu interfase, buble sort & quick sort akan implement interface sortAlgorithmm
 * jadi ketika object Binary search dipanggil maka buat object interface lalu deklasi sorting apa yg akan di gunakan.
 */
public interface SortAlgorithm {
	public int [] sort(int[]number);

}
