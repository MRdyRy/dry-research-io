package com.rudyr.research.io.ext.algorithm;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.rudyr.research.io.ext.impl.SortAlgorithm;

@Component
@Primary
public class BubbleSortAlgorithm implements SortAlgorithm{
	
	public int [] sort(int [] number){
		//algorithm bubble sort
		return number;
	}

}
