package com.rudyr.research.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.rudyr.research.io.ext.BinarySearchImpl;
import com.rudyr.research.io.ext.algorithm.BubbleSortAlgorithm;

@SpringBootApplication
public class RdyResearchIoApplication {

	public static void main(String[] args) {
		
		ApplicationContext applicationContext = SpringApplication.run(RdyResearchIoApplication.class, args);

//		for (String name : applicationContext.getBeanDefinitionNames()) {
//			System.out.println("bean definition name : "+name);
//		}

		BinarySearchImpl binarySearch = applicationContext.getBean(BinarySearchImpl.class);
		int number = binarySearch.binarySearch(new int[] {1,2,3,4,5,6}, 0); 
		System.out.println(number);
	}

}
